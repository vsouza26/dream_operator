# Dream Operator

Robocode robot. Has a GuessFactor gun and a simple tracking movement with wall smoothing.

## GuessFactor gun

The hardest part of the robot to understand. The idea behind is to use the diference between the inital position and the 
actual position of the enemy robot to calculate an offset. This offset is then used in a _traditional heads-on gun_. There is also segmentation of distance to differentiate between longer distances and shorter distances. *GREAT GUN*. _(I could do better, i think)_

## Simple Iterative Wall Smoothing

The easiest part of the robot that wasn't straight foward. I had to reasearch a bit before i undestood the idea behind this snippet of code. It tries to track heads on the enemy robot and, when very close to a wall, tries to smooth the moviment and not collide. I inserted a limit of how close my robot can actually be from some enemy robot. _(I will try to iterate)_

## Radar

The radar is simply a 3 lines of code that calculate the absolute bearing of the enemy robot and the offset of the radar to this angle. Very Simple, but effective.


## CREDITS

- GuessFactor Targeting Tutorial -  KAWIGI
- a PEZ (Simple Iterative Wall Smoothing)
- Talking Heads (for the name)
