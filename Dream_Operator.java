/*
 * Créditos 
 *	a Kawigi (GuessFactor Targeting Tutorial)
 * 	a PEZ (Simple Iterative Wall Smoothing)
 */

package tvieira;
import robocode.*;
import java.util.*;
import robocode.util.Utils;
import java.awt.geom.*;
import java.util.Random;
public class Dream_Operator extends AdvancedRobot
{
	//Codigo da arma
	private class WaveBullet
	{
		private double startX, startY, startBearing, power;
		private long   fireTime;
		private int    direction;
		private int[]  returnSegment;
	
		public WaveBullet(double x, double y, double bearing, double power,
			int direction, long time, int[] segment)
		{
			startX         = x;
			startY         = y;
			startBearing   = bearing;
			this.power     = power;
			this.direction = direction;
			fireTime       = time;
			returnSegment  = segment;
		}
		public double getBulletSpeed()
		{
			return 20 - power * 3; //Velocidade da bala disparada pelo meu tanque
		}
	
		public double maxEscapeAngle()
		{
			return Math.asin(8 / getBulletSpeed()); //maior escape que pode ser feito pelo tanque inimigo
		}
		public boolean checkHit(double enemyX, double enemyY, long currentTime)
		{
			if (Point2D.distance(startX, startY, enemyX, enemyY) <= 
					(currentTime - fireTime) * getBulletSpeed())
			{
				double desiredDirection = Math.atan2(enemyX - startX, enemyY - startY); //angulo do vetor do tanque inimigo 
																						//inicialmente ao vetor do tanque inimigo
																						//atual
				double angleOffset = Utils.normalRelativeAngle(desiredDirection - startBearing); //Diferença entre os dois
																									//angulos
				double guessFactor =
					Math.max(-1, Math.min(1, angleOffset / maxEscapeAngle())) * direction; //fator do palpite calculado
																							//em relação ao angulo de escape maximo
				int index = (int) Math.round((returnSegment.length - 1) / 2 * (guessFactor + 1)); //achar o index que corresponde ao
																								//palpite correto
				out.println("index: " + index);
				returnSegment[index]++; //aumenta mais um nesse palpite
				return true;
			}
			return false;
		}
	} 
	List<WaveBullet> waves = new ArrayList<WaveBullet>(); //arrayList dessas balas
	static int[][] stats = new int[13][31]; //  segmentação das situações
	int direction = 1; //direção padrão do movimento

	public void run() {
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);

    	do {
			turnRadarRightRadians(Double.POSITIVE_INFINITY);
        	scan();
    	} while (true);
	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		double absBearing = getHeadingRadians() + e.getBearingRadians(); //Angulo do vetor que sai do meu tanque ao tanque inimigo
        double radarTurn = absBearing - getRadarHeadingRadians(); //O quanto o radar precisa girar para ver o tanque inimigo
		double angle = Utils.normalRelativeAngle(radarTurn); //angulo
    	setTurnRadarRightRadians(2.0 * angle);
		//variaveis da movimentação
		double _oppEnergy = 100.0;
		// variaveis da arma
		double ex = getX() + Math.sin(absBearing) * e.getDistance();
		double ey = getY() + Math.cos(absBearing) * e.getDistance();
		// Processando as ondas
		for (int i=0; i < waves.size(); i++)
		{
			WaveBullet currentWave = (WaveBullet)waves.get(i);
			if (currentWave.checkHit(ex, ey, getTime()))
			{
				waves.remove(currentWave);
				i--;
			}
		}
		double power = Math.min(3, Math.max(.1, 3));
		// Se a velocidade for igual a 0 use o já estava antes
		if (e.getVelocity() != 0)
		{
			if (Math.sin(e.getHeadingRadians()-absBearing)*e.getVelocity() < 0)
				direction = -1;
			else
				direction = 1;
		}
		int[] currentStats = stats[(int)(e.getDistance() / 100)]; //inici
		WaveBullet newWave = new WaveBullet(getX(), getY(), absBearing, power,
                        direction, getTime(), currentStats);
		int bestindex = 15;	// Inicializa os palpites, palpite de 0.
		for (int i=0; i<31; i++)
			if (currentStats[bestindex] < currentStats[i])
				bestindex = i;
		out.println("Stats length: " + stats.length);
		// para calcular o palpite pelo index:
		double guessfactor = (double)(bestindex - (stats[(int)(e.getDistance() / 100)].length - 1) / 2)
                        / ((stats[(int)(e.getDistance() / 100)].length - 1) / 2);
		out.println("bestindex " + bestindex);
		out.println("guessfactor " + guessfactor);
		double angleOffset = direction * guessfactor * newWave.maxEscapeAngle();
                double gunAdjust = Utils.normalRelativeAngle(
                        absBearing - getGunHeadingRadians() + angleOffset);
                setTurnGunRightRadians(gunAdjust);
		if (getGunHeat() == 0 && gunAdjust < Math.atan2(9, e.getDistance()) && setFireBullet(power) != null){
                        waves.add(newWave);
		}
		//Termino de codigo da arma
		//Movimento simples de evitar a parede e direcionar para o robo inimigo
		double goalDirection = absBearing-Math.PI/2 * (double) direction;
		Rectangle2D fieldRect = new Rectangle2D.Double(18, 18, getBattleFieldWidth()-36,
    		getBattleFieldHeight()-36);
		while (!fieldRect.contains(getX()+Math.sin(goalDirection)*120, getY()+
        	Math.cos(goalDirection)*120))
		{
			goalDirection += direction*.1;	
		}
		double turn =
    		robocode.util.Utils.normalRelativeAngle(goalDirection-getHeadingRadians());
		if(e.getDistance() > 200) // nao chegar muito perto
		{
			if (Math.abs(turn) > Math.PI/2)
			{
				turn = robocode.util.Utils.normalRelativeAngle(turn + Math.PI);
				setBack(100);
			}
			else
				setAhead(100);
		}
		setTurnRightRadians(turn);
		

        _oppEnergy = e.getEnergy();

		execute();
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
	}	
}


